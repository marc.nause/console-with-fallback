/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * See <a href="https://codeberg.org/marc.nause/console-with-fallback/issues/14">
 * https://codeberg.org/marc.nause/console-with-fallback/issues/14</a> for requirements.
 */
class Issue14Test {

    @Test
    void readLineTest() {

        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader("abc")), new PrintWriter(new StringWriter()));

        assertEquals("abc", characterDevice.readLine());
        assertNull(characterDevice.readLine());
    }

    @Test
    void readLineLongTest() {

        final StringBuilder sb = new StringBuilder();
        IntStream.range(0, 100).forEach((i) -> sb.append("abc_").append(i).append(System.lineSeparator()));

        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader(sb.toString())), new PrintWriter(new StringWriter()));

        IntStream.range(0, 100).forEach((i) -> assertEquals("abc_" + i, characterDevice.readLine()));
        assertNull(characterDevice.readLine());
    }

    @Test
    void readLineWithOutputTest() {

        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader("abc")), new PrintWriter(new StringWriter()));

        assertEquals("abc", characterDevice.readLine("xyz"));
        assertNull(characterDevice.readLine("cxu"));
    }

    @Test
    void readPasswordTest() {

        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader("abc")), new PrintWriter(new StringWriter()));

        assertArrayEquals("abc".toCharArray(), characterDevice.readPassword());
        assertNull(characterDevice.readPassword());
    }

    @Test
    void readPasswordWithOutputTest() {

        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader("abc")), new PrintWriter(new StringWriter()));

        assertArrayEquals("abc".toCharArray(), characterDevice.readPassword("xyz"));
        assertNull(characterDevice.readPassword("cxu"));
    }

}
