/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import java.io.IOException;
import java.io.Reader;

class IoExceptionThrowingReader extends Reader {

    @Override
    public void close() {
        // do nothing
    }

    @Override
    public int read(char[] arg0, final int arg1, final int arg2) throws IOException {
        throw new IOException();
    }

}
