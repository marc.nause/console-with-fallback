/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ConsoleCreatorTest {

    @Test
    void notNullTest() {

        assertNotNull(ConsoleCreator.console());
    }

    @Test
    void correctTypeTest() {

        final Console console = ConsoleCreator.console();

        if (System.console() == null) {
            assertTrue(console instanceof CharacterDevice);
        } else {
            assertTrue(console instanceof ConsoleDevice);
        }
    }

    @Test
    void testThreadSafety() {
        final Collection<Integer> consoleHashes = new HashSet<>();

        IntStream.range(0, 1000000)
                .parallel()
                .forEach(i -> consoleHashes.add(ConsoleCreator.console().hashCode()));

        assertEquals(1, consoleHashes.size());
    }

}
