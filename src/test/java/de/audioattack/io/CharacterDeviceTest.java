/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CharacterDeviceTest {

    @Test
    void testConstructorNullReader() {

        final PrintWriter printWriter = new PrintWriter(new StringWriter());
        assertThrows(NullPointerException.class, () -> new CharacterDevice(null, printWriter));
    }

    @Test
    void testConstructorNullWriter() {

        final BufferedReader reader = new BufferedReader(new StringReader(""));
        assertThrows(NullPointerException.class, () -> new CharacterDevice(reader, null));
    }

    @Test
    void testReader() throws IOException {

        final StringReader stringReader = new StringReader(("abc"));
        final BufferedReader bufferedReader = new BufferedReader(stringReader);
        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader,
                new PrintWriter(new StringWriter()));

        assertNotNull(characterDevice.reader());
        final char[] cbuff = new char[16];
        final int len = characterDevice.reader().read(cbuff);
        assertArrayEquals("abc".toCharArray(), Arrays.copyOf(cbuff, len));
    }

    @Test
    void testWriter() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader("")),
                printWriter);

        assertNotNull(characterDevice.writer());
        characterDevice.printf("test");
        assertEquals("test", stringWriter.getBuffer().toString());
    }

    @Test
    void testFormat() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(""));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        characterDevice.format("Test %s %s", "test1", "test2");

        assertEquals("Test test1 test2", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintf() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(""));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        characterDevice.printf("Test %s %s", "test1", "test2");

        assertEquals("Test test1 test2", stringWriter.getBuffer().toString());
    }

    @Test
    void testReadLineStringObjectArray() {

        final String input = "testtesttest";

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(input));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final String result = characterDevice.readLine("Test %s %s", "test1", "test2");

        assertEquals("Test test1 test2", stringWriter.getBuffer().toString());
        assertEquals(input, result);
    }

    @Test
    void testReadLine() {

        final String input = "testtesttest";

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(input));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final String result = characterDevice.readLine();

        assertEquals(0, stringWriter.getBuffer().length());
        assertEquals(input, result);
    }

    @Test
    void testReadLineWithException() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new IoExceptionThrowingReader());

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final String result = characterDevice.readLine();

        assertNull(result);
    }

    @Test
    void testReadPasswordStringObjectArray() {

        String linebreak = System.lineSeparator();

        final String input = "testtesttest";

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(input));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final char[] result = characterDevice.readPassword("Test %s %s", "test1", "test2");

        assertEquals("WARNING! Password will be visible in the command line!" + linebreak + "Test test1 test2",
                stringWriter.getBuffer().toString());
        assertArrayEquals(input.toCharArray(), result);
    }

    @Test
    void testReadPassword() {

        String linebreak = System.lineSeparator();

        final String input = "testtesttest";

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(input));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final char[] result = characterDevice.readPassword();

        assertEquals("WARNING! Password will be visible in the command line!" + linebreak,
                stringWriter.getBuffer().toString());
        assertArrayEquals(input.toCharArray(), result);
    }

    @Test
    void testFormatWithNull() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(""));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);

        assertThrows(NullPointerException.class, () -> characterDevice.format(null));
    }

    @Test
    void testReadPasswordWithNull() {

        final String input = "testtesttest";

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(input));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);

        assertThrows(NullPointerException.class, () -> characterDevice.readPassword(null));
    }

}
