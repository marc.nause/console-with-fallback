package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * See <a href="https://codeberg.org/marc.nause/console-with-fallback/issues/33">
 * https://codeberg.org/marc.nause/console-with-fallback/issues/33</a> for requirements.
 */
class Issue33Test {

    @Test
    void testPrint() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(""));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final ConsolePrintDecorator decorator = new ConsolePrintDecorator(characterDevice);
        int finished = 0;
        decorator.print("\rFINISHED " + finished + "%");

        assertEquals("\rFINISHED 0%", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintln() {

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        final BufferedReader bufferedReader = new BufferedReader(new StringReader(""));

        final CharacterDevice characterDevice = new CharacterDevice(bufferedReader, printWriter);
        final ConsolePrintDecorator decorator = new ConsolePrintDecorator(characterDevice);
        int finished = 0;
        decorator.println("FINISHED " + finished + "%");

        assertEquals("FINISHED 0%" + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

}
