/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertFalse;

/**
 * See <a href="https://codeberg.org/marc.nause/console-with-fallback/issues/13">
 * https://codeberg.org/marc.nause/console-with-fallback/issues/13</a> for requirements.
 */
class Issue13Test {

    @Test
    void readerTest() throws IOException {

        final TestReader testReader = new TestReader();
        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(testReader), new PrintWriter(new StringWriter()));
        final Reader reader = characterDevice.reader();

        reader.close();
        assertFalse(testReader.closed.get());
    }

    @Test
    void writerTest() {

        final TestWriter testWriter = new TestWriter();
        final CharacterDevice characterDevice = new CharacterDevice(new BufferedReader(new StringReader("")), testWriter);
        final PrintWriter writer = characterDevice.writer();

        writer.close();
        assertFalse(testWriter.closed.get());
    }

    private static class TestReader extends Reader {

        final AtomicBoolean closed = new AtomicBoolean(false);

        @Override
        public int read(final char[] cbuff, final int off, final int end) {
            return -1;
        }

        @Override
        public void close() {
            closed.set(true);
        }
    }

    private static class TestWriter extends PrintWriter {

        private final AtomicBoolean closed = new AtomicBoolean(false);

        public TestWriter() {
            super(new StringWriter());
        }

        @Override
        public void close() {
            closed.set(true);
        }
    }
}
