/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */
package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * See <a href="https://codeberg.org/marc.nause/console-with-fallback/issues/12">
 * https://codeberg.org/marc.nause/console-with-fallback/issues/12</a> for requirements.
 */
class Issue12Test {

    @Test
    void testThreadsPrintf() throws InterruptedException {

        final StringWriter stringWriter = new StringWriter();
        final CharacterDevice characterDevice =
                new CharacterDevice(
                        new BufferedReader(new StringReader("")),
                        new PrintWriter(stringWriter));

        final char[] a = new char[16];
        final char[] b = new char[a.length];
        Arrays.fill(a, 'A');
        Arrays.fill(b, 'B');

        final Thread thread1 = new Thread(() -> IntStream.range(0, 10000).forEach(i -> characterDevice.printf(new String(a))));
        final Thread thread2 = new Thread(() -> IntStream.range(0, 10000).forEach(i -> characterDevice.printf(new String(b))));

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        final char[] c = new char[16];
        for (int i = 0; i < stringWriter.getBuffer().length(); i += c.length) {

            stringWriter.getBuffer().getChars(i, i + c.length, c, 0);

            if (!Arrays.equals(a, c) && !Arrays.equals(b, c)) {
                fail("Mixed output found.");
            }
        }
    }

    @Test
    void testThreadsReadLine() throws InterruptedException {

        final char[] c = new char[60000];
        final String s = "AB\n";
        for (int i = 0; i < c.length; i++) {
            Arrays.fill(c, i, i + 1, s.charAt(i % 3));
        }

        final StringBuilder sb1 = new StringBuilder();
        final StringBuilder sb2 = new StringBuilder();

        final StringWriter stringWriter = new StringWriter();
        final CharacterDevice characterDevice =
                new CharacterDevice(
                        new BufferedReader(new StringReader(new String(c))),
                        new PrintWriter(stringWriter));

        final Thread thread1 = new Thread(() -> IntStream.range(0, 10000).forEach(i -> sb1.append(characterDevice.readLine())));
        final Thread thread2 = new Thread(() -> IntStream.range(0, 10000).forEach(i -> sb2.append(characterDevice.readLine())));

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();

        final char[] b = "AB".toCharArray();
        final char[] a = new char[b.length];
        for (int i = 0; i < sb1.length(); i += a.length) {

            sb1.getChars(i, i + a.length, a, 0);

            if (!Arrays.equals(a, b)) {
                fail("Mixed input found.");
            }
        }

        for (int i = 0; i < sb2.length(); i += a.length) {

            sb2.getChars(i, i + a.length, a, 0);

            if (!Arrays.equals(a, b)) {
                fail("Mixed input found.");
            }
        }
    }

}
