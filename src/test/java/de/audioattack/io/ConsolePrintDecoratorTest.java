/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */
package de.audioattack.io;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ConsolePrintDecoratorTest {

    private ConsolePrintDecorator consolePrintDecorator;
    private StringWriter stringWriter;
    private BufferedReader bufferedReader;

    @BeforeEach
    void setUp() {
        stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        bufferedReader = new BufferedReader(new StringReader("test test test"));

        consolePrintDecorator = new ConsolePrintDecorator(new CharacterDevice(bufferedReader, printWriter));
    }

    @Test
    void testReader() {
        assertNotNull(consolePrintDecorator.reader());
    }

    @Test
    void testWriter() {
        assertNotNull(consolePrintDecorator.writer());
    }

    @Test
    void testFormat() {
        consolePrintDecorator.format("Test %s %s", "test1", "test2");

        assertEquals("Test test1 test2", stringWriter.getBuffer().toString());
    }

    @Test
    void testFormatWithLocale() {
        final ConsolePrintDecorator returnValue =
                consolePrintDecorator.format(Locale.GERMANY, "Test %.2f %d", 1.5, 100_000_000L);
        assertEquals("Test 1,50 100000000", stringWriter.getBuffer().toString());
        assertSame(consolePrintDecorator, returnValue);
    }

    @Test
    void testPrintInteger() {
        consolePrintDecorator.print(123);
        assertEquals("123", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintLong() {
        consolePrintDecorator.print(Long.MAX_VALUE);
        assertEquals("" + Long.MAX_VALUE, stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintChar() {
        consolePrintDecorator.print('c');
        assertEquals("c", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintFloat() {
        consolePrintDecorator.print(1.5f);
        assertEquals("" + 1.5f, stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintCharArray() {
        final String s = "qwertzASDFGHyxcvbn";
        consolePrintDecorator.print(s.toCharArray());
        assertEquals("qwertzASDFGHyxcvbn", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintDouble() {
        consolePrintDecorator.print(Double.MIN_NORMAL);
        assertEquals("" + Double.MIN_NORMAL, stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintObject() {
        final Object o = new Object();
        consolePrintDecorator.print(o);
        assertEquals(o.toString(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintObjectNull() {
        consolePrintDecorator.print((Object) null);
        assertEquals("null", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintBoolean() {
        consolePrintDecorator.print(true);
        assertEquals("" + true, stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintString() {
        consolePrintDecorator.print("Ice cream!");
        assertEquals("Ice cream!", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintStringNull() {
        consolePrintDecorator.print((String) null);
        assertEquals("null", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintf() {
        consolePrintDecorator.printf("Test %s %s", "test1", "test2");
        assertEquals("Test test1 test2", stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintfWithLocale() {
        final ConsolePrintDecorator returnValue =
                consolePrintDecorator.printf(Locale.GERMANY, "Test %.2f %d", 1.5, 100_000_000L);
        assertEquals("Test 1,50 100000000", stringWriter.getBuffer().toString());
        assertSame(consolePrintDecorator, returnValue);
    }

    @Test
    void testPrintln() {
        consolePrintDecorator.println();
        assertEquals(System.lineSeparator(), stringWriter.getBuffer().toString());
        consolePrintDecorator.println();
        assertEquals(System.lineSeparator() + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnInt() {
        consolePrintDecorator.println(123);
        assertEquals(123 + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnChar() {
        consolePrintDecorator.println('c');
        assertEquals('c' + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnLong() {
        consolePrintDecorator.println(Long.MIN_VALUE);
        assertEquals(Long.MIN_VALUE + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnFloat() {
        consolePrintDecorator.println(6.5f);
        assertEquals(6.5f + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnCharArray() {
        final String s = "äöüß";
        consolePrintDecorator.println(s.toCharArray());
        assertEquals(s + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnDouble() {
        consolePrintDecorator.println(Double.MAX_VALUE);
        assertEquals(Double.MAX_VALUE + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnObject() {
        final Object o = new Object();
        consolePrintDecorator.println(o);
        assertEquals(o + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnString() {
        consolePrintDecorator.println("xyz");
        assertEquals("xyz" + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnStringNull() {
        consolePrintDecorator.println((String) null);
        assertEquals("null" + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testPrintlnBoolean() {
        consolePrintDecorator.println(false);
        assertEquals(false + System.lineSeparator(), stringWriter.getBuffer().toString());
    }

    @Test
    void testReadLine() {
        final String result = consolePrintDecorator.readLine();
        assertEquals(0, stringWriter.getBuffer().length());
        assertEquals("test test test", result);
    }

    @Test
    void testReadLineWithText() {
        final String result = consolePrintDecorator.readLine("Test %s %s", "test1", "test2");
        assertEquals("Test test1 test2", stringWriter.getBuffer().toString());
        assertEquals("test test test", result);
    }

    @Test
    void readPassword() {
        final char[] result = consolePrintDecorator.readPassword();
        assertArrayEquals("test test test".toCharArray(), result);
    }

    @Test
    void testReadPassword() {
        final char[] result = consolePrintDecorator.readPassword("aaaaa");
        assertArrayEquals("test test test".toCharArray(), result);
    }

    @Test
    void testFlush() {

        stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(new BufferedWriter(stringWriter));
        bufferedReader = new BufferedReader(new StringReader("test test test"));

        consolePrintDecorator = new ConsolePrintDecorator(new CharacterDevice(bufferedReader, printWriter));

        consolePrintDecorator.append('a');
        assertEquals("", stringWriter.toString());
        consolePrintDecorator.flush();
        assertEquals("a", stringWriter.toString());
    }

    @Test
    void appendChar() {
        consolePrintDecorator.append('A');
        assertEquals("A", stringWriter.getBuffer().toString());
        consolePrintDecorator.append('x').append('Y');
        assertEquals("AxY", stringWriter.getBuffer().toString());
    }

    @Test
    void testAppendCharSequence() {
        consolePrintDecorator.append("AAA");
        assertEquals("AAA", stringWriter.getBuffer().toString());
        consolePrintDecorator.append("x").append("YYY");
        assertEquals("AAAxYYY", stringWriter.getBuffer().toString());
    }

    @Test
    void testAppendCharSequenceNull() {
        consolePrintDecorator.append(null);
        assertEquals("null", stringWriter.getBuffer().toString());
    }

    @Test
    void testAppendCharSequenceWithRange() {
        consolePrintDecorator.append("ABC", 1, 2);
        assertEquals("B", stringWriter.getBuffer().toString());
        consolePrintDecorator.append("xyz", 0, 1).append("QAY", 2, 3);
        assertEquals("BxY", stringWriter.getBuffer().toString());
    }

    @Test
    void testAppendCharSequenceWithRangeNull() {
        consolePrintDecorator.append(null, 1, 3);
        assertEquals("ul", stringWriter.getBuffer().toString());
    }

    @Test
    void testCheckError() {

        final Writer throwingWriter = new Writer() {
            @Override
            public void write(final char[] chars, final int i, final int i1) throws IOException {
                throw new IOException();
            }

            @Override
            public void flush() {
                // do nothing
            }

            @Override
            public void close() {
                // do nothing
            }
        };
        final PrintWriter printWriter = new PrintWriter(throwingWriter);
        bufferedReader = new BufferedReader(new StringReader("test test test"));

        consolePrintDecorator = new ConsolePrintDecorator(new CharacterDevice(bufferedReader, printWriter));
        assertFalse(consolePrintDecorator.checkError());

        consolePrintDecorator.print("abc");

        assertTrue(consolePrintDecorator.checkError());
    }
}