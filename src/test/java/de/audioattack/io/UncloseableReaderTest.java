/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.Reader;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UncloseableReaderTest {

    @Test
    void testRead() throws IOException {
        try (final TestReader testReader = new TestReader();
             final UncloseableReader uncloseableReader = new UncloseableReader(testReader)) {

            assertFalse(testReader.read.get());
            assertEquals(3, uncloseableReader.read(new char[0], 10, 13));
            assertTrue(testReader.read.get());
        }
    }

    @Test
    void testClose() {

        final TestReader testReader = new TestReader();
        assertFalse(testReader.closed.get());

        final UncloseableReader uncloseableReader = new UncloseableReader(testReader);
        uncloseableReader.close();
        assertFalse(testReader.closed.get());

        testReader.close();
        assertTrue(testReader.closed.get());
    }

    private static class TestReader extends Reader {

        final AtomicBoolean closed = new AtomicBoolean(false);
        final AtomicBoolean read = new AtomicBoolean(false);

        @Override
        public int read(final char[] cbuff, final int off, final int end) {
            read.set(true);
            return end - off;
        }

        @Override
        public void close() {
            closed.set(true);
        }
    }
}