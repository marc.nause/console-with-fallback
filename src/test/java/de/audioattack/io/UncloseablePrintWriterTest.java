/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import org.junit.jupiter.api.Test;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UncloseablePrintWriterTest {

    @Test
    void testClose() {
        try (final UncloseablePrintWriterTest.TestWriter testWriter = new UncloseablePrintWriterTest.TestWriter(new StringWriter());
             final UncloseablePrintWriter uncloseablePrintWriter = new UncloseablePrintWriter(testWriter)) {

            assertFalse(testWriter.closed.get());
            uncloseablePrintWriter.close();
            assertFalse(testWriter.closed.get());
            testWriter.close();
            assertTrue(testWriter.closed.get());
        }
    }

    @Test
    void testWrite() {
        final StringWriter stringWriter = new StringWriter();

        try (final UncloseablePrintWriterTest.TestWriter testWriter = new UncloseablePrintWriterTest.TestWriter(stringWriter);
             final UncloseablePrintWriter uncloseablePrintWriter = new UncloseablePrintWriter(testWriter)) {

            assertEquals("", stringWriter.getBuffer().toString());
            uncloseablePrintWriter.write("test");
            assertEquals("test", stringWriter.getBuffer().toString());
            uncloseablePrintWriter.write("test", 2, 2);
            assertEquals("testst", stringWriter.getBuffer().toString());
        }
    }

    private static class TestWriter extends PrintWriter {

        private final AtomicBoolean closed = new AtomicBoolean(false);

        public TestWriter(final Writer writer) {
            super(writer);
        }

        @Override
        public void close() {
            closed.set(true);
        }
    }
}