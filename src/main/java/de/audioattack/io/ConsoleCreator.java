/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.Charset;

/**
 * Creates an implementation of {@link Console de.audioattack.io.Console} which wraps
 * {@link java.io.Console java.io.Console} or which provides a fallback implementation
 * in case the former is not available.
 *
 * @since 1.0.0
 */
public final class ConsoleCreator {

    private static final Console CONSOLE = System.console() == null ? createCharacterDevice()
            : new ConsoleDevice(System.console());

    private ConsoleCreator() {
    }

    /**
     * Gets an implementation of {@link Console de.audioattack.io.Console}. Implementation based on
     * {@link java.io.Console java.io.Console} is preferred.
     *
     * @return the console
     */
    @SuppressFBWarnings(
            value = "MS_EXPOSE_REP",
            justification = "This seems to be a false positive in Spotbugs, "
                    + "see: https://github.com/spotbugs/spotbugs/issues/1747")
    public static Console console() {
        return CONSOLE;
    }

    @SuppressWarnings("squid:S106")
    private static Console createCharacterDevice() {
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, Charset.defaultCharset()));
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(System.out, Charset.defaultCharset()), true);
        return new CharacterDevice(reader, writer);
    }
}
