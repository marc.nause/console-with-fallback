/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import java.io.IOException;
import java.io.Reader;
import java.util.Objects;

/* default */ class UncloseableReader extends Reader {

    private final Reader reader;

    UncloseableReader(final Reader reader) {
        this.reader = Objects.requireNonNull(reader, "reader must not be <null>");
    }

    @Override
    public int read(final char[] cbuff, final int off, final int len) throws IOException {
        return reader.read(cbuff, off, len);
    }

    @Override
    public void close() {
        // ignore close
    }
}
