/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import java.io.PrintWriter;
import java.io.Writer;

/* default */ class UncloseablePrintWriter extends PrintWriter {

    UncloseablePrintWriter(final Writer writer) {
        super(writer, true);
    }

    @Override
    public void close() {
        // ignore close
    }
}
