/*
 * SPDX-FileCopyrightText: 2021 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

/**
 * Provides a drop-in replacement for Java's {@link java.io.Console java.io.Console} which is problematic to use
 * since it is not available in some environments and is a common cause of {@link java.lang.NullPointerException}s.
 */
package de.audioattack.io;
