/*
 * SPDX-FileCopyrightText: 2018 Marc Nause <marc.nause@gmx.de>
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package de.audioattack.io;

import java.io.PrintWriter;
import java.io.Reader;

/**
 * Console which is a wrapper of the Java Console class.
 */
/* default */ class ConsoleDevice implements Console {

    private final java.io.Console console;

    /**
     * Constructor.
     *
     * @param console used to read and write characters
     */
    ConsoleDevice(final java.io.Console console) {
        this.console = console;
    }

    @Override
    public Reader reader() {
        return console.reader();
    }

    @Override
    public PrintWriter writer() {
        return console.writer();
    }

    @Override
    public Console format(final String fmt, final Object... args) {
        console.format(fmt, args);
        return this;
    }

    @Override
    public Console printf(final String fmt, final Object... args) {
        console.printf(fmt, args);
        return this;
    }

    @Override
    public String readLine(final String fmt, final Object... args) {
        return console.readLine(fmt, args);
    }

    @Override
    public String readLine() {
        return console.readLine();
    }

    @Override
    public char[] readPassword(final String fmt, final Object... args) {
        return console.readPassword(fmt, args);
    }

    @Override
    public char[] readPassword() {
        return console.readPassword();
    }

    @Override
    public void flush() {
        console.flush();
    }

}
