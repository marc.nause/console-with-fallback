Badges provided by [![./badges](dotslash-badges.svg)](https://codeberg.org/lhinderberger/dot-slash-badges/) under the following license:

>To the extent possible under law, the author has waived all copyright and
related or neighboring rights to the pre-built badges originally distributed
alongside ./badges, as described in CC0 1.0 Universal.
>
>For details see CC0-1.0.txt or https://creativecommons.org/publicdomain/zero/1.0/.