# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.2](https://codeberg.org/marc.nause/console-with-fallback/compare/1.2.0...1.2.1) - 2025-02-07
### Changed:
- Update maven plugins
### Fixed:
- Fix link to bug tracker in pom.xml

## [1.2.1](https://codeberg.org/marc.nause/console-with-fallback/compare/1.2.0...1.2.1) - 2024-02-17
### Fixed:
- Prevent leakage of internal objects

## [1.2.0](https://codeberg.org/marc.nause/console-with-fallback/compare/1.1.3...1.2.0) - 2021-12-08
### Changed:
- Make class `CharacterDevice` public

## [1.1.3](https://codeberg.org/marc.nause/console-with-fallback/compare/1.1.2...1.1.3) - 2021-07-17
### Changed:
- Add badges to README.md
- Improve documentation

### Fixed:
- Make sure that of fallback returns identical instances of `Reader` and `Writer` in the respective methods

## [1.1.2](https://codeberg.org/marc.nause/console-with-fallback/compare/1.1.1...1.1.2) - 2021-06-16
### Fixed:
- Add missing flush when using print methods of `ConsolePrintDecorator`

## [1.1.1](https://codeberg.org/marc.nause/console-with-fallback/compare/1.1.0...1.1.1) - 2021-06-10
### Fixed:
- Remove bug in print methods of `ConsolePrintDecorator` which caused exceptions printing special characters

## [1.1.0](https://codeberg.org/marc.nause/console-with-fallback/compare/1.0.0...1.1.0) - 2021-05-06
### Added:
- This changelog
- `ConsolePrintDecorator` which provides several convenience methods

### Fixed:
- `Reader` and `Writer` returned from fallback must not be closeable
- Prevent potential `NullPointerException` in `readPassword` methods of fallback
- Ensure that read methods of fallback return `null` when end of the input stream is reached

## [1.0.0](https://codeberg.org/marc.nause/console-with-fallback/src/tag/1.0.0) - 2021-04-19
Initial release
