[![Apache 2.0 License](badges/license-apache20.svg)](LICENSE)
[![API Reference](badges/docs-apireference.svg)](https://www.javadoc.io/doc/de.audioattack.io/console/latest/)
[![Issue Tracker](badges/community-issuetracker.svg)](https://codeberg.org/marc.nause/console-with-fallback/issues)
![Status: Active](badges/status-active.svg)
[![Distribution: Maven Central](badges/distribution-mavencentral.svg)](https://search.maven.org/artifact/de.audioattack.io/console)

# Console with Fallback
Java 1.6 introduced the [`java.io.Console` class](https://docs.oracle.com/javase/6/docs/api/java/io/Console.html) which provides methods to access the character-based console device, if any, associated with the current Java virtual machine.

The class has many advantages over using `System.out`, but unfortunately using `Console` in an IDE leads to ugly `NullPointerException`s (see [Stackoverflow for examples](https://stackoverflow.com/search?q=system.console+NullPointerException)). This is the reason many developers avoid `Console` (if they even know about the class).

*Console with Fallback* provides a drop-in replacement for `java.io.Console` which falls back to `System.out` and `System.in` in case it is used in an environment where the system does not provide a native console. It was inspired by the blog article [Java: System.console(), IDEs and testing](https://illegalargumentexception.blogspot.com/2010/09/java-systemconsole-ides-and-testing.html).

# Usage
The Java implementation of the `Console` class can easily be replaced by simply replacing the imports, and the way the object is created.

Example code with Java implementation:

```java
package de.audioattack.example;

import java.io.Console;

public class Example {

    public static void main(String... args) {
        Console console = System.console();
        console.printf("Hello world!");
    }
}
```

Example code with _Console with Fallback_:

```java
package de.audioattack.example;

import de.audioattack.io.Console;
import de.audioattack.io.ConsoleCreator;
import de.audioattack.io.ConsolePrintDecorator;

public class Example {

    public static void main(String... args) {
        Console console = ConsoleCreator.console();
        console.printf("Hello world!");

        // Use ConsolePrintDecorator for more convenient output methods.
        ConsolePrintDecorator printDecorator = new ConsolePrintDecorator(console);
        printDecorator.println("Hello world!");
    }
}
```

# Releases
The latest release is available on [Maven Central Repository](https://central.sonatype.com/artifact/de.audioattack.io/console).

Maven dependency:

```
<dependency>
  <groupId>de.audioattack.io</groupId>
  <artifactId>console</artifactId>
  <version>1.2.2</version>
</dependency>
```

Gradle dependency:

```
implementation("de.audioattack.io:console:1.2.2")
```

# Documentation
The API documentation can be viewed online on [javadoc.io](https://www.javadoc.io/doc/de.audioattack.io/console/latest/).

# Collaboration
The preferred way of contributing to this project is via [Codeberg](https://codeberg.org). Alternatively patches can be 
submitted via email ([marc.nause@gmx.de](mailto:marc.nause@gmx.de)).

# License
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.